# == Class: androidlab::params
#
#
# Default parameters
class androidlab::params {

    $version                    = '24.4.1'
    $install_dir                = '/usr/local/android'
    $master_source              = true
    $google_url                 = 'https://dl.google.com/android/'
    $base_source_path           = 'puppet:///modules/androidlab/'

    case $::architecture {
        /x86_64|amd64|x64/  :   { $arch = 'x64' }
        'x86'               :   { $arch = 'i586' }
        default             :   { fail("My module doesn't support that architecture ${::architecture}") }
    }

    case $::operatingsystem {
        'lion', 'mountain lion', 'mavericks', 'Darwin', 'yosemite', 'el capitan': {
            $destination_path   = '/tmp'
            $instalation_user   = 'root'
            $instalation_group  = 'root'
            $so_ref             = 'macosx'
            $base_package_name  = 'android-sdk_r'
            $file_format        = 'zip'
        }
        'ubuntu': {
            $destination_path   = '/tmp'
            $instalation_user   = 'root'
            $instalation_group  = 'root'
            $file_format        = 'tgz'
            $so_ref             = 'linux'
            $base_package_name  = 'android-sdk_r'
        }
        'windows' : {
            $destination_path   = 'C:\\Program Files\\'
            $file_format        = 'exe'
            $so_ref             = 'windows'
            $base_package_name  = 'installer_r'
        }
        default: {
            fail("My module does not support that operative system: ${::operatingsystem}")
        }
    }
}
