# Class: androidlab
# ===========================
#
# Full description of class androidlab here.
#
# Parameters
# ==Class: androidlab
#
#   Install android sdk
#
class androidlab (
    $version            = $androidlab::params::version,
    $master_source      = $androidlab::params::master_source
) inherits androidlab::params {

    #The package name
    $install_dir        = $androidlab::params::install_dir
    $package_name       = "$base_package_name$version-$androidlab::params::so_ref"
    include androidlab::requirements
    include androidlab::download
    include androidlab::install
    Class['androidlab::requirements'] -> Class['androidlab::download'] -> Class['androidlab::install']
}