# == Class: androidlab::download
#
#   Download the androidsdk

class androidlab::download(
    $base_master        = $androidlab::params::base_source_path,
    $base_url           = $androidlab::params::google_url,
    $package_name       = $androidlab::package_name,
    $file_format        = $androidlab::params::file_format,
    $from_master        = $androidlab::master_source,
    $destination_path   = $androidlab::destination_path,
    $install_dir        = $androidlab::params::install_dir
) {

    if $from_master {
        $final_source = "$base_master$package_name.$file_format"
    }
    else {
        $final_source = "$base_url$package_name.$file_format"
    }

    case $::operatingsystem {
        'windows' : {
            if $from_master {
                file { "${destination_path}${package_name}.${file_format}":
                    source      => "${final_source}"
                }
            }
            else {
                pget {"Download ${final_source}":
                    source      => "${final_source}",
                    target      => "${destination_path}",
                }
            }
        }
        'ubuntu' : {
            if $from_master {
               file { "${destination_path}/${package_name}.${file_format}":
                    source      => "${final_source}",
                }
                archive::extract {"${package_name}":
                    src_target          => "${destination_path}",
                    target              => "${install_dir}",
                    extension           => "${file_format}",
                    strip_components    => 1,
                    ensure              => present,
                    before              => Exec['Update alternatives for android']
                }
            }
            else {
                archive {"${package_name}":
                    url                 => "${final_source}",
                    src_target          => "${destination_path}",
                    target              => "${install_dir}",
                    extension           => "${file_format}",
                    strip_components    => 1,
                    ensure              => present,
                    checksum            => false,
                    before              => Exec['Update alternatives for android']
                }
            }
        }
        default : {
            if $from_master {
               file { "${destination_path}/${package_name}.${file_format}":
                    source      => "${final_source}",
                }
            }
            else {
                exec {"Download ${package_name}":
                    command => "curl -L ${final_source} -o ${destination_path}/${package_name}.${file_format}", 
                    path    => ["/bin", "/usr/bin", "/usr/sbin"]
                }
            }
            archive::extract {"${package_name}":
                src_target          => "${destination_path}",
                target              => "${install_dir}",
                extension           => "${file_format}",
                strip_components    => 1,
                ensure              => present,
            }
        }
    }
}