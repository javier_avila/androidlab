# == Class: androidlab::alternatives
#
#   Set alternatives (Only for ubuntu nodes)
#
# 
class androidlab::alternatives(
    $install_dir    = $androidlab::install_dir,
    $package_name   = $androidlab::package_name

) {
    $android_home   = "$install_dir/tools"
    exec { 'Update alternatives for android':
        provider    => shell,
        command     => "update-alternatives --install /usr/bin/android android ${android_home}/android 10000"
    }
}