# == Class: androidlab::install
#
# Install the jdk package
#
class androidlab::install {
    case $::operatingsystem {
        'windows' : {
            package { 'install jdk ${androidlab::package_name}':
                source      => "${androidlab::destination_path}${androidlab::package_name}.${androidlab::params::file_format}",
                ensure      => installed,
                install_options => ['/S']
            }
        }
        'ubuntu' : {
            include androidlab::alternatives
        }
    }
}