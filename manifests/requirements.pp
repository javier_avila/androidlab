# == Class: androidlab:requirements
#
# Install the requirements

class androidlab::requirements {
    case $::operatingsystem {
        'ubuntu' : {
            ensure_packages(['libc6-i386', 'lib32stdc++6', 'lib32gcc1', 'lib32ncurses5', 'lib32z1'])
        }
    }
}
