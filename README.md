# androidlab

#### Table of Contents

1. [Overview](#overview)
2. [Module Description](#Module Description)
3. [Setup - The basics of getting started with javalab](#setup)
4. [Usage - Configuration options and additional functionality](#usage)
5. [Limitations - OS compatibility, etc.](#limitations)

## Overview

This module is a multiplatform android-sdk intaller, it can install from the
master puppet node or from the google webpage .

## Module Description

It can install the packages from the master puppet node or from the oracle webpage .


## Setup

Before start ensure the cyberious-pget, camptocamp-archive and puppetlabs-stdlib 
are installed in your puppet master node.

If you want to use your puppet master node to distribute the packages, you have to
download the require packages into files folder.

### Beginning with androidlab

The default usage is

```
class { 'androidlab'}
```


You can customize parameters when declaring the androidlab class. For instance, this declaration
install androidsdk from the puppet master.

```
class { 'androidlab':
    master_source => true,
}
```


## Usage

The default androidlab class sets up the default package to 24.4.1

You can customize the androidsdk version that you want to install. For instance this delcaration it will installs the
package 1.6_r1 from the master node.


```
class { 'androidlab':
    version => '1.6_r1'
}
```


## Limitations

You shoud not use this module in a production environment (yet).
It was tested in ubuntu14:04, windows 10, windows 7, windows 8, mac os (darwin)